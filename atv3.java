package ex1;
import java.util.ArrayList;

import java.util.Scanner;
import java.math.*;
public class atv3 {

	 
//	Escreva um programa que leia um número inteiro positivo n e gere um array com os 'n' primeiros números primos.
	
		public static void main(String[] args) {
				Scanner sc = new Scanner(System.in);
				
				
				System.out.println("Digite um número inteiro positivo:");
		        int num = sc.nextInt();
		        
		       
		        if (num <= 0) {
		            System.out.println("O número deve ser positivo.");
		            return; 
		        }
		        
		        
		        int[] primeirosPrimos = new int[num];
		         int n = 2; 
		        int count = 0; 
		        
		        while (count < num) {
		            if (primo(n)) {
		                primeirosPrimos[count] = n;
		                count++;
		            }
		            n++;
		        }
		        
		        
		        System.out.println("Os " + num + " primeiros números primos são:");
		        for (int i = 0; i < num; i++) {
		            System.out.print(primeirosPrimos[i]);
		            if (i < num - 1) {
		                System.out.print(", ");
		            }
		        }
		        
		      
		        
		    }
		    
		    
		    public static boolean primo(int numero) {
		        if (numero <= 1) {
		            return false;
		        }
		        for (int i = 2; i <= Math.sqrt(numero); i++) {
		            if (numero % i == 0) {
		                return false;
		            }
		        }
		        return true;
		    }
		}

		

	


	


