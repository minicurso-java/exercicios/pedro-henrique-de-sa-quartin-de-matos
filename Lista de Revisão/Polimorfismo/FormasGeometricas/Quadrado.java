import java.util.Locale;
import java.util.Scanner;

public class Quadrado extends FiguraGeometrica {

        public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		FiguraGeometrica quadrado = new FiguraGeometrica();

            System.out.println("Digite o lado do seu quadrado: ");
            quadrado.lado = sc.nextDouble();

            System.out.println("A area do seu quadrado é = "+quadrado.AreaQuadrado());
           
            System.out.println("O perimetro do seu quadrado é = "+quadrado.PerimetroQuadrado());
}
}
