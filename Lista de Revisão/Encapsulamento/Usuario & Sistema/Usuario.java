public class Usuario {
    private String nome;
    private String senha;
    private String email;

    
      // Métodos para acessar os atributos
    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public String getSenha() {
        return senha;
    }

    // Métodos para modificar os atributos
    public void setNome(String novoNome) {
        this.nome = novoNome;
    }

    public void setEmail(String novoEmail) {
        this.email = novoEmail;
    }

    public void setSenha(String novaSenha) {
        this.senha = novaSenha;
    }
   
    
}



