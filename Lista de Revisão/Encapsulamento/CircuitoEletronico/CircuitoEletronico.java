public class CircuitoEletronico {
    private double tensao; 
    private double corrente; 

    
    public CircuitoEletronico(double tensao, double corrente) {
        this.tensao = tensao;
        this.corrente = corrente;
    }

    // Métodos para acessar os atributos
    public double getTensao() {
        return tensao;
    }

    public double getCorrente() {
        return corrente;
    }

    // Método para calcular a resistência do circuito 
    public double calcularResistencia() {
        if (corrente != 0) {
            return tensao / corrente;
        } else {
            return Double.POSITIVE_INFINITY; // Se a corrente for zero, a resistência é infinita
        }
    }

    // Método para calcular a potência dissipada no circuito 
    public double calcularPotencia() {
        return tensao * corrente;
    }
}



