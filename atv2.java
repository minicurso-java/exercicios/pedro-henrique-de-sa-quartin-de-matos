package ex1;
import java.util.ArrayList;

import java.util.Scanner;
import java.math.*;
public class atv2 {

	 
//	Crie um vetor de inteiros com tamanho 8 e solicite ao usuário que informe dois índices de elementos que deverão ser trocados de posição. 
//	Imprima o vetor resultante.
	
		public static void main(String[] args) {
				Scanner sc = new Scanner(System.in);
				
				
				int [] vetor = new int [8];
				System.out.println("Digite duas posições para efetuar a troca (entre 0 e 7): ");
				int pos1 = sc.nextInt();
				int pos2 = sc.nextInt();	
				if (pos1 < 0 || pos1 >= vetor.length || pos2 < 0 || pos2 >= vetor.length) {
		            System.out.println("Índices inválidos. Os índices devem estar entre 0 e 7.");
		            return;
				}
				System.out.println("Digite os valores dos números:");
		        for (int i = 0; i < vetor.length; i++) {
		            vetor[i] = sc.nextInt();
		        }
		        int temp = vetor[pos1];
		        vetor[pos1] = vetor[pos2];
		        vetor[pos2] = temp;
		        
		        System.out.print("Resultado: [");
		        for (int i = 0; i < vetor.length; i++) {
		            System.out.print(vetor[i]);
		            if (i < vetor.length - 1) {
		                System.out.print(", ");
		            }
		        }
		        System.out.println("]");
				
			}
		}

		

	


	


