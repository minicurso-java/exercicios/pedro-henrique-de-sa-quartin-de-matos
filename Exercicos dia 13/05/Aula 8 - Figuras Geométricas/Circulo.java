import java.util.Locale;
import java.util.Scanner;

public class Circulo extends FiguraGeometrica {
public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		FiguraGeometrica circulo = new FiguraGeometrica();

        System.out.println("Digite o raio do circulo: ");
        circulo.raio = sc.nextDouble();

        System.out.println("A area do seu circulo é = "+circulo.AreaCirulo());
           
        System.out.println("O perimetro do seu circulo é = "+circulo.PerimetroCirulo());
}
}