
class FiguraGeometrica {
    public String nome;
    public double raio;
    public double altura;
    public double lado;
    public double lado2;
    public double lado3;
    
    
  public double AreaQuadrado () {
      return lado * lado;
	}
  public double AreaTriangulo (){
    return lado * altura / 2;
  }
  public double AreaCirulo (){
    return Math.PI * raio * raio;
  }
  public double PerimetroQuadrado () {
    return lado * 4;
}
  public double PerimetroTriangulo (){
  return lado + lado2 + lado3;
}
  public double PerimetroCirulo (){
  return Math.PI * 2 * raio;
}
  

}
