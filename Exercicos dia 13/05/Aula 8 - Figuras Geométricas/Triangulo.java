import java.util.Locale;
import java.util.Scanner;

public class Triangulo extends FiguraGeometrica {
public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		FiguraGeometrica triangulo = new FiguraGeometrica();

        System.out.println("Digite a base do seu triangulo: ");
        triangulo.lado = sc.nextDouble();

        System.out.println("Digite um lado do seu triangulo: ");
        triangulo.lado2 = sc.nextDouble();

        System.out.println("Digite o outro lado do seu triangulo: ");
        triangulo.lado3 = sc.nextDouble();

        System.out.println("Digite a altura do seu triangulo: ");
        triangulo.altura = sc.nextDouble();

            System.out.println("A area do seu triangulo é = "+triangulo.AreaTriangulo());
            
            System.out.println("O perimetro do seu triangulo é = "+triangulo.PerimetroTriangulo());
}
}
