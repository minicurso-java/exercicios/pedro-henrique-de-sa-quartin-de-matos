import java.util.Locale;
import java.util.Scanner;

public class Programa extends ContaBancaria {
public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		ContaBancaria produto = new ContaBancaria();

        int p;
        do{
		System.out.println("Qual o número da sua conta: ");
		produto.numero = sc.nextInt();
        System.out.println("Qual o primeiro nome do titular da conta: ");
		produto.titular = sc.next();
        

        System.out.println();
		System.out.println("Dados da conta:");
		System.out.println("    " + produto.titular + ", " + produto.numero + " \n    Saldo: $ " + produto.SaldoTotal());
		
        //Deposito
        System.out.println();
		System.out.println("Quanto você deseja depositar: ");
		double saldo = sc.nextDouble();
		produto.Deposito(saldo);
		System.out.println();
	
		
        // Saque
        System.out.println();
		System.out.println("Quanto você deseja sacar: ");
		saldo = sc.nextDouble();
		produto.Sacar(saldo);
		System.out.println();
		
		
        // Loop
		
        System.out.println("Digite 1 para realizar mais uma transação ou qualquer outro número para sair!");
         p = sc.nextInt();

        }while (p == 1);

        System.out.println("Atualização dos Dados: " + produto.titular + ", " + produto.numero + " \n Saldo: $ " + produto.SaldoTotal());

		}
}




