
import java.util.Locale;
import java.util.Scanner;



    public class Programa extends Aluno{
        public static void main(String[] args) {
            Locale.setDefault(Locale.US);
            Scanner sc = new Scanner(System.in);
            Aluno calculo = new Aluno();
            int i, p = 0;

            do{
            System.out.println("Digite seu primeiro nome: ");
            calculo.nome = sc.nextLine();
            System.out.println("Digite sua matricula: ");
            calculo.matricula = sc.nextInt();
            System.out.println("Qual é a disciplina: ");
            calculo.disciplinas = sc.next();
            System.out.println("Digite o número de provas da disciplina "+calculo.disciplinas+": ");
            calculo.provas = sc.nextInt();

                //Sistema para a confirmação/loop
                System.out.println("\nNome: "+calculo.nome+"\nMatricula: "+calculo.matricula+"\nDisciplina: "+calculo.disciplinas+"\nProvas: "+calculo.provas);
                System.out.println("\nPara confirmar seus dados pressione '1'\nCaso deseje refazer digite '0'");
                i = sc.nextInt();
                while(i != 0 && i != 1){
                    System.out.println("Digite um valor válido!");
                    System.out.println("\nPara confirmar seus dados pressione '1'\nCaso deseje refazer digite '0'");
                    i = sc.nextInt();
                }

            }while (i == 0);
                    
               
                while (p != calculo.provas){
                    p++;
                    System.out.println("Digite sua nota: ");
                    double notas = sc.nextDouble();
                    calculo.somaTotal(notas);
                }

                if(calculo.calcularMedia() >= 7)
                    System.out.println("APROVADO!");
                else
                    System.out.println("REPROVADO!");
                
                System.out.println("A sua média nessa disciplina foi = "+calculo.calcularMedia());
        }
    }

        
        